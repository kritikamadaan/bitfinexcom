import React from 'react'
import DonutChart from 'react-d3-donut';
import { connect } from 'react-redux';
import { getStocks } from '../../../redux/selectors';

function Chart(props) {
    const {price,quantity,investedAmount,netInvested}= props;
    const avgCost = investedAmount/quantity;
    const marketValue = price * quantity;
    const pl = marketValue - investedAmount;
    const percentageReturn = (pl * 100)/marketValue;
    const data = [{
        count: 40  ,  
        color: '#03a9f4', 
        name: 'Mutual Funds'
        },
        {
        count : 20 ,     
        color :' #AE9C46',  
        name: 'ETFs'
        }]
    return (
        <div className="chartcard">
        <h4>Portfolio</h4>
        <p>Mutual funds</p>
        <DonutChart
        innerRadius={70}
        outerRadius={100}
        transition={true}
        svgClass="example6"
        pieClass="pie6"
        displayTooltip={true}
        strokeWidth={3}
        data={data} />
        <p>Mutual funds: </p>
        <p>ETFs:</p>
        </div>
    )
}
const mapStateToProps = state => ({ stocks: getStocks(state) });
export default connect(mapStateToProps)(Chart)
