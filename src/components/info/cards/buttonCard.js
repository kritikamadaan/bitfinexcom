import React from 'react';

function Button(props) {
  return (
    <div className="card card-quantity" style={{display: 'flex', justifyContent: 'center', flexDirection: 'column', alignItems: 'center'}}>
      <button className='btn-buy-sell'>Buy</button>
      <button className='btn-buy-sell'>Sell</button>  
    </div>
  );
}

export default Button;
