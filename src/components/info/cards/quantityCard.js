import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faDatabase, faAt, faMoneyBill } from '@fortawesome/free-solid-svg-icons'

function Quantity(props) {
  const {
    quantity,
    investedAmount,
  } = props.data;

  const avgCost = investedAmount/quantity;

  return (
    <div className="card card-quantity">
      <div className="container">
        <section className='col'>
          <div>
            <FontAwesomeIcon style={{width: '25px', paddingRight: '10px'}} icon={faDatabase} />
            <span>
              Quantity
            </span>
          </div>
          <div>  
            <FontAwesomeIcon style={{width: '25px', paddingRight: '10px'}} icon={faAt} />
            <span>
              Avg. Cost
            </span>
          </div>
          <div>
            <FontAwesomeIcon style={{width: '25px', paddingRight: '10px'}} icon={faMoneyBill} />
            <span>
              Invested Amt.
            </span>
          </div>
        </section>
        <section className='col'>
          <span style={{fontWeight: 'bold'}}>
            {quantity}
          </span>
          <span style={{fontWeight: 'bold'}}>
            {avgCost.toFixed(2)}
          </span>
          <span style={{fontWeight: 'bold'}}>
            {investedAmount.toFixed(2)}
          </span>
        </section>
      </div>
    </div>
  );
}

export default Quantity;
