import React from 'react';

function Ticker(props) {
  const {
    ticker,
    price,
    index,
  } = props.data;

  return (
    <div className="card card-ticker">
      <div className="container">        
        <section className='col'>
          <span className='ticker'>
            {ticker}
          </span>
          <span className='price'>${price}</span>
        </section>
        <section className='col'>
          <span className='index'>{index}</span>
          <span className='equity'>US Equity</span>
        </section>
      </div>
    </div>
  );
}

export default Ticker;
