import React from 'react';
import TickerCard from './cards/tickerCard';
import QuantityCard from './cards/quantityCard';
import PortfolioCard from './cards/portfolioCard';
import PLCard from './cards/PLCard';
import ButtonCard from './cards/buttonCard';
import Chart from './cards/Chart';

function Info(props) {
  const {
    ticker,
    price,
    index,
    quantity,
    investedAmount,
    netInvested,
  } = props.data;

  return (
    <div className="info">
      <section className="cards">
        <TickerCard data={{ticker, price, index}}/>
        <QuantityCard data={{quantity, investedAmount}}/>
        <PortfolioCard data={{price, quantity, investedAmount, netInvested}}/>
        <PLCard data={{price, quantity, investedAmount}}/>       
        <ButtonCard/>
      </section>
    </div>
  );
}

export default Info;
