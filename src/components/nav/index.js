import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars } from '@fortawesome/free-solid-svg-icons'


function Nav(props) {
  const {
    showMenu,
  } = props;

  return (
    <nav className='nav'>
      <section className='brand'>
        darts
      </section>
      <section className='nav-right'>
        <FontAwesomeIcon icon={faBars} style={{cursor: "pointer"}} onClick={showMenu}/>
      </section>
    </nav>
  );
}

export default Nav;
