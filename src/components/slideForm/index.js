import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowRight } from '@fortawesome/free-solid-svg-icons'


function SlideForm(props) {
  const {
    closeMenu,
  } = props;

  return (
    <div className='slide-form' style={props.style}>
      <FontAwesomeIcon icon={faArrowRight} className='slide-back' onClick={closeMenu}/>
      {props.children}
    </div>
  );
}



export default SlideForm;

