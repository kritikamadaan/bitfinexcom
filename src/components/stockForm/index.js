import React, { useState } from 'react';
import { connect } from 'react-redux';
import { getStockObject } from '../../redux/selectors';
import { modifyContent } from '../../redux/actions';

function StockForm(props) {
  const {
    stock,
  } = props;
  const [ticker, setTicker] = useState(undefined);
  const [price, setPrice] = useState(undefined);
  const [quantity, setQuantity] = useState(undefined);
  const [investedAmount, setInvestedAmount] = useState(undefined);
  
  const onChangeSelect = event => {
    const tickr = event.target.value;
    setTicker(tickr);
    setPrice(stock[tickr].price);
    setQuantity(stock[tickr].quantity);
    setInvestedAmount(stock[tickr].investedAmount);
  }

  const onFormSubmit = event => {
    event.preventDefault();
    if (ticker === undefined) {
      return;
    }
    props.modifyContent({
      ticker,
      price: parseFloat(price),
      quantity: parseInt(quantity),
      investedAmount: parseFloat(investedAmount),
    });
  }

  return (
    <form className='stock-form' style={props.style} onSubmit={onFormSubmit}>
      <select onChange={onChangeSelect}>
        <option disabled selected value>Select...</option>
        {
          Object.keys(stock).map(ticker => (
            <option key={ticker}>{ticker}</option>
          ))
        }
      </select>
      <label htmlFor='price'>
        Price
      </label>
      <input type='text' id='price' name='price' value={price} onChange={e => setPrice(e.target.value)}/>
      <label htmlFor='quantity'>
        Quantity
      </label>
      <input type='text' id='quantity' name='quantity' value={quantity} onChange={e => setQuantity(e.target.value)}/>
      <label htmlFor='investedAmount'>
        Invested Amount
      </label>
      <input type='text' id='investedAmount' name='investedAmount' value={investedAmount} onChange={e => setInvestedAmount(e.target.value)}/>
      <button>SUBMIT</button>
    </form>
  );
}


const mapStateToProps = state => ({ stock: getStockObject(state) });
export default connect(mapStateToProps, { modifyContent })(StockForm);
