import { MODIFY_CONTENT } from "./actionTypes";

export const modifyContent = content => ({
  type: MODIFY_CONTENT,
  payload: {
    ...content
  }
});
