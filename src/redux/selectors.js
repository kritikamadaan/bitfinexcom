export const getStocks = store => {
  const { stocks } = store;
  const calcSum = (sum, curr) => {
    const { investedAmount } = curr;
    return sum+investedAmount;
  }

  const netInvested = stocks.reduce(calcSum, 0);

  return stocks.map(stock => ({...stock, netInvested}));
};

export const getStockObject = store => {
  const { stocks } = store;
  const out = {};
  const makeOutObject = (stock) => {
    out[stock.ticker] = {
      ...stock,
    }
  };

  stocks.forEach(makeOutObject);
  return out;
}